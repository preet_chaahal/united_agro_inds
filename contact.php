<!doctype html>
<html lang="en">
  <head>
  	<title>United Agro Inds</title>
  	
  	<!-- meta info -->
  	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<!-- /meta-info -->
    <link rel="shortcut icon" href="favicon.ico">
  	<!-- bootstrap css file -->
  	 <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
  	<!-- //bootstrap css file -->
  	
  	<!-- including style.css file -->
  	 <link rel="stylesheet" type="text/css" href="css/style.css" />
  	<!-- //style.css file -->
  	<!-- Fontawesome icons -->
  		<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
  	<!-- //Fontawesome icons -->
    <!-- loading Montserrat font  -->
      <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <!-- //Mosterrat font -->
  </head>
  <body>
  <div data-parallax="scroll" data-image-src="images/cow-lg-2.jpg">
  <!-- site header -->
    <?php require_once('includes/header.php'); ?>

  <div class="container-fluid">
    <div class="row map max-width-950">

    <style>.embed-container { position: relative; padding-bottom: 36.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; } .embed-container iframe{pointer-events: none;}</style><div class='embed-container'><iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27403.207360122684!2d75.66740704999997!3d30.84744844999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x391a7fb9ade8a31b%3A0x45b54d01fcf0f569!2sMullanpur+Dakha%2C+Punjab+141101!5e0!3m2!1sen!2sin!4v1436682273855' width='800' height='600' frameborder='0' style='border:0' allowfullscreen></iframe></div>  
    </div><!-- //row-fluid -->
  </div><!-- /container-fluid -->
  <div class="container-fluid">
      <div class="row contact-form max-width-950">
          
          <form method="post">
          
            <h2 class="text-center">SEND US A MESSAGE</h2>
          
            <div class="col-sm-6">
          
              <div class="form-group">
                <label for="message" class="sr-only">Your Message</label>
                <textarea class="form-control" rows="7" id="message" placeholder="Your Message"></textarea>
              </div>
          
            </div><!-- //col-sm-6 -->

            <div class="col-sm-6">
          
              <div class="form-group">
                <label for="name" class="sr-only">Your Name</label>
                <input type="text" id="name" class="form-control" placeholder="Your Name" />
              </div>
          
              <div class="form-group">
                <label for="name" class="sr-only">Your Email</label>
                <input type="email" id="email" class="form-control" placeholder="Your Email" />
              </div>
          
              <div class="form-group text-center">
                <input type="button" class="btn" id="submit" class=" btn" value="Send Message">
              </div>
              <div class="form-group text-center">
              <p id="returnmessage"></p>
              </div>
          
            </div><!-- //col-sm-6 -->
          
          </form>
      </div><!-- //row -->
  </div><!-- //container-fluid -->  
  <!-- site-footer -->
    <?php require_once('includes/footer.php'); ?>
  <!-- //site-footer -->

  <!-- jquery js file -->
    <script type="text/javascript" src="js/jquery.js"></script>
  <!-- //jquery file -->
  <!-- loading gif -->
    <?php require_once('includes/loader.php') ?>
  <!-- //loader -->
    <!-- //site-header -->
    <script type="text/javascript" src="js/loader.js"></script>
  <!-- //loader -->

  <!-- bootstrap js file -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
  <!-- /bootstrap js file -->
  <!-- Email script -->
  <script type="text/javascript" src="js/email.js"></script>
  <!-- //email script -->
  <!-- parallax effect -->
    <script language="javascript" src="js/parallax.min.js"></script>
  <!-- //parallax effect -->
  </body>
</html>