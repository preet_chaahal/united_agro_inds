<!doctype html>
<html lang="en">
  <head>
  	<title>United Agro Inds</title>
  	
  	<!-- meta info -->
  	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<!-- /meta-info -->
    <link rel="shortcut icon" href="favicon.ico">
  	<!-- bootstrap css file -->
  	 <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
  	<!-- //bootstrap css file -->
  	
  	<!-- including style.css file -->
  	 <link rel="stylesheet" type="text/css" href="css/style.css" />
  	<!-- //style.css file -->
  	<!-- Fontawesome icons -->
  		<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
  	<!-- //Fontawesome icons -->
  	<!-- loading Montserrat font  -->
  		<link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
  	<!-- //Mosterrat font -->
  </head>
  <body>
  <div data-parallax="scroll" data-image-src="images/cow-lg-2.jpg">
  <!-- site header -->
    <?php require_once('includes/header.php'); ?>
  <!-- //site-header -->
  <!-- loading gif -->
    <?php require_once('includes/loader.php') ?>
  <!-- //loader -->

  <div class="container-fluid">
    <div class="row max-width-950 results-content">
      <div class="col-xs-12">
		
		<h2 class="text-center">Results for Lucky Draw dated 15th January, 2017</h2>
		<h4 class="text-center">Venue: BLESSING RESORTS, RAIKOT</h4>
		<div class="result-img">
			<img src="images/draw-agroinds.jpg" class="img-responsive" />
		</div><!-- //result-img -->
		
	  </div>
	  <div class="clearfix"></div>
	  <hr>
	  <br>
	  <div class="col-xs-12">
		<h2 class="text-center">Results for Lucky Draw dated 19th July, 2015</h2>
		<h4 class="text-center">Venue: ROYAL GREEN RESORTS, RAIKOT-SARABHA ROAD, TAJPUR, LUDHIANA</h4>
		<div class="result-img">
			<img src="images/results.png" class="img-responsive" />
		</div><!-- //result-img -->
	  </div><!-- //col-xs-12 -->
	</div><!-- //row -->
  </div><!-- //max-width-950 -->
  <!-- site-footer -->
  <?php require_once('includes/footer.php'); ?>
  <!-- //site-footer -->
  <!-- jquery js file -->
    <script type="text/javascript" src="js/jquery.js"></script>
  <!-- //jquery file -->
      <!-- loader gif file -->
  <script type="text/javascript" src="js/loader.js"></script>
  <!-- //loader -->

  <!-- bootstrap js file -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
  <!-- /bootstrap js file -->
    <!-- parallax effect -->
    <script language="javascript" src="js/parallax.min.js"></script>
  <!-- //parallax effect -->
  </body>
</html>