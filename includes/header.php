<!-- code to make active link active --><?php
function active($currect_page){
  $url_array =  explode('/',$_SERVER['REQUEST_URI']) ;
  $url = end($url_array);  
  if($currect_page == $url){
      echo 'active-link'; //class name in css 
  }
  else{
    echo 'non-active-link';
    } 
}
?>
<!-- End of code to make active link active throughout -->
<header>
	<div class="container-fluid">
		<div class="row max-width-950 top-div">
			<div class="logo-wrap col-xs-3 col-md-3">
				<img src="images/site-logo-2.png" />
			</div>
			<div class="col-xs-7 col-md-7 top-head">
				<h1 class="text-center">United Agro Industries</h1>
				<p class="text-center">Mfrs. of: SPECIAL CHURI & HIGH QUALITY
				CATTLE FEED</p>
			</div>
			<div class="col-xs-2 col-md-2">
				<img class="img-responsive" src="images/ganesh.jpg">
				<!--<div style="float: right"><a href="./punjabi/index.php">Punjabi</a></div>-->
			</div>
		</div><!-- //row -->
	</div>
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    
	    <div class="max-width-950">
	
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <!--<a class="navbar-brand" href="index.php"></a>-->
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse main-nav" id="bs-example-navbar-collapse-1">
		      
		      <ul class="nav navbar-nav">
		        <li><a class="<?php active('index.php');?>" href="index.php">Home <span class="sr-only">(current)</span></a></li>
		        <li><a class="<?php active('about.php');?>" href="about.php">About Us</a></li>
		        <li><a class="<?php active('products.php');?>" href="products.php">Our Products</a></li>
		        <li><a class="<?php active('contact.php');?>" href="contact.php">Contact Us</a></li>
		      	<li><a class="<?php active('results.php');?>" href="results.php">Results</a></li>
		      </ul>
		      
		    </div><!-- /.navbar-collapse -->
	  	
	  	</div><!-- /main-header -->

	  </div><!-- /.container-fluid -->
	</nav>
</header>