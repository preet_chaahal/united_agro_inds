<div class="container-fluid">
  	
  	<div class="max-width-950 contact-div row">
  		
  		<h2>Reach Us</h2>
  		<div class="col-md-4 btm-links">

      <h3>Quick Links</h3>
      <ul class="btm-ul">
        <li><a href="index.php">Home</a></li>
        <li><a href="about.php">About</a></li>
        <li><a href="products.php">Our Products</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>

      </div>
      <div class="col-md-4 social-links">
  			<h3>Social Links</h3>
  			<a href="#"><i class="fa fa-facebook"></i></a>
  			<a href="#"><i class="fa fa-youtube-play"></i></a>
  			<a href="#"><i class="fa fa-google-plus"></i></a>
  		</div><!-- //social-links -->
  		<div class="col-md-4">
  			<h3>Address</h3>
  			<address>United Agro Industries<br>
  			Opposite New Grain Market,<br>
  			Raikot Road<br>
  			Mullanpur (Ldh.)<br>
  			+91-98761-00739<br>
        +91-98762-00739<br>
        +91-98147-17485<br>
        +91-81462-00739</address>
  		</div><!-- //col-md-4 -->
  			
  	</div><!-- contact-div -->
  
  </div><!-- contact-div-fluid -->
<footer class="container-fluid">
	<div class="max-width-950 copyright">
		<p>&copy; <?php echo date('Y'); ?>,United Agro Inds. , Mullanpur(Pb.) . 
		All rights reserved</p>
	</div><!-- //max-width-950 -->
</footer>