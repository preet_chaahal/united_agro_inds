<!doctype html>
<html lang="en">
  <head>
  	<title>United Agro Inds</title>
  	
  	<!-- meta info -->
  	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<!-- /meta-info -->
  	<link rel="shortcut icon" href="favicon.ico">
  	<!-- bootstrap css file -->
  	 <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
  	<!-- //bootstrap css file -->
  	
  	<!-- including style.css file -->
  	 <link rel="stylesheet" type="text/css" href="css/style.css" />
  	<!-- //style.css file -->
  	<!-- Fontawesome icons -->
  		<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
  	<!-- //Fontawesome icons -->
  	<!-- loading Montserrat font  -->
  		<link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
  	<!-- //Mosterrat font -->
  </head>
  <body>
<div data-parallax="scroll" data-image-src="images/cow-lg-2.jpg">
  <!--<div class="row-fluid main-logo text-center">
  	<img src="images/site-logo.png" />
  </div>-->
  <!-- site header -->
    <?php require_once('includes/header.php'); ?>
  <!-- //site-header -->
  <!-- loading gif -->
  	<?php require_once('includes/loader.php') ?>
  <!-- //loader -->

  <div class="container-fluid cow-bg">	  
	<div class="row max-width-950">
		  
		  <!-- carousel -->
		  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-example-generic" data-slide-to="1"></li>
				<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				<li data-target="#carousel-example-generic" data-slide-to="3"></li>
				<li data-target="#carousel-example-generic" data-slide-to="4"></li>
			</ol>

		    <!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				    
			    <div class="item active">
			      <img src="images/slider-1.jpg" class="img-responsive" alt="...">
			      <!--<div class="carousel-caption">
			        ...
			      </div>-->
			    </div>
			    <div class="item">
			      <img src="images/slider-2.jpg" class="img-responsive" alt="...">
			    </div>
			    <div class="item">
			      <img src="images/slider-3.jpg" class="img-responsive" alt="...">
			    </div>
			    <div class="item">
			      <img src="images/slider-4.jpg" class="img-responsive" alt="...">
			    </div>
			    <div class="item">
			      <img src="images/slider-5.jpg" class="img-responsive" alt="...">
			    </div>

			</div><!-- carousel-inner -->

		  </div><!-- //carousel-example-generic -->
		  <!-- //carousel -->
	</div><!-- //max-width-1600 -->
  </div><!-- //row-fluid -->

  <div class="container-fluid word-abt-company-wrap">
  	<div class="max-width-950 word-abt-company">
	  	
	  	<h2><em>A word about the company</em></h2>
	  	<p><b>United Agro Industries(U A I)</b> is the proud manufacturer of <em>SPECIAL CHURI AND
        HIGH QUALITY FEED</em>. U A I was established in 2000 and started its operations at village 
        Gondwal(Ludhiana). Right from the incorporation of the company the prior aim of U A I is
        to produce the best quality cattle feed.
	  	</p>
	 	
	 	<p><a href="about.php" class="text-center btn btn-lg">More</a></p>
	</div><!-- //max-width-950 -->
  </div><!-- //row-fluid -->

  <div class="container-fluid">
  		<div class="max-width-950 upcoming-events">
  			<h1 class="text-center">Upcoming Events</h1>
  			<h4 class="text-center">Lucky Draw 19th July, 2015</h4>
  			<div class="events-desc">
	  			
	  			<h5>Prizes</h5>	
	  			<ul>
	  				<li><b>Ist Prize: </b><l>Alto K-10 Car </l> <b>Qty:</b> 1 <b>Description:</b>5 Digits</li>
	  				<li><b>IInd Prize: </b>Bullet Motorcycle <b>Qty:</b> 5 <b>Description:</b>5 Digits</li>
	  				<li><b>IIIrd Prize: </b>Platina Motorcycle <b>Qty:</b> 10 <b>Description:</b>One out of all series,5 Digits</li>
	  				<li><b>IVth Prize: </b>Sunflame Gas Stove <b>Qty:</b> 30, <b>Description:</b>Three out of all series,5 Digits</li>
	  				<li><b>Vth Prize: </b>Blanket Double Bed <b>Qty:</b> 120, <b>Description:</b>Last Four digits in all series,5 Digits</li>
	  				<li><b>VIth Prize: </b>Electric Press <b>Qty:</b> 300, <b>Description:</b>Last three digits in all series,5 Digits</li>
	  				<li><b>VIIth Prize: </b>Casrol Set <b>Qty:</b> 600, <b>Description:</b>Last three digits in all series,5 Digits</li>
	  				<li><b>VIIIth Prize: </b>Lunch Box <b>Qty:</b> 1000, <b>Description:</b>Last three digits in all series,5 Digits</li>
	  				<li><b>IXth Prize: </b>Jug <b>Qty:</b> 4000, <b>Description:</b>last two digits in all series,5 Digits</li> 
				</ul>  			
  			</div><!-- //events-desc -->
  			<h5>Venue</h5>
  				<p>Royal Green Resorts, Raikot-Sarabha Road, Tajpur, Ludhiana</p>
  			
  		</div><!-- //upcoming-events -->
  </div><!-- //container-fluid -->
  
  <div class="container-fluid">

    <div class="max-width-950 word-abt-company row">
	  	
		<div class="products">
			<h2>Our Products</h2>
			<div class="col-sm-4 products-div">
				
				<div class="thumbnail">
						<img src="images/mari-gold.png" class="img-responsive" alt="">
					<div class="caption">
						<h3><a href="products.php">Marrie Gold</a></h3>
						<p>Marrie Gold Feed is prepared using latest technologies available and is
              a pure and balanced feed for cows and buffalloes.</p>
					</div><!-- //caption -->
				</div><!-- //thumbnail -->
			
			</div><!-- col-sm-4 -->
			<div class="col-sm-4 products-div">
				
				<div class="thumbnail">
						<img src="images/milk-star.png" class="img-responsive" alt="">
					<div class="caption">
						<h3><a href="products.php">Milk Star</a></h3>
						<p>Milk Star Feed is prepared using latest technologies available and is
              a pure and balanced feed for cows and buffalloes.</p>
					</div><!-- //caption -->
				</div><!-- //thumbnail -->
			
			</div><!-- col-sm-4 -->
			<div class="col-sm-4 products-div">
				
				<div class="thumbnail">
						<img src="images/mega-star.png" class="img-responsive" alt="">
					<div class="caption">
						<h3><a href="products.php">Mega Star</a></h3>
						<p>Milk Star Feed is prepared using latest technologies available and is
              a pure and balanced feed for cows and buffalloes.</p>
					</div><!-- //caption -->
				</div><!-- //thumbnail -->
			
			</div><!-- col-sm-4 -->
		
		</div><!-- //products -->

	</div><!-- //word-abt-company -->	
  
  </div><!-- //container-fluid -->

  <!-- site-footer -->
    <?php require_once('includes/footer.php'); ?>
  <!-- //site-footer -->
</div> <!-- content wrap for parrallax effect -->
  <!-- jquery js file -->
  	<script type="text/javascript" src="js/jquery.js"></script>
  <!-- //jquery file -->
      <!-- loader gif file -->
  <script type="text/javascript" src="js/loader.js"></script>
  <!-- //loader -->

  <!-- bootstrap js file -->
  	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
  <!-- /bootstrap js file -->
  <!-- parallax effect -->
  	<script language="javascript" src="js/parallax.min.js"></script>
  <!-- //parallax effect -->
  </body>
</html>