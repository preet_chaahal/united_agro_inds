<!doctype html>
<html lang="en">
  <head>
  	<title>United Agro Inds</title>
  	
  	<!-- meta info -->
  	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<!-- /meta-info -->

  	<!-- bootstrap css file -->
  	 <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
  	<!-- //bootstrap css file -->
  	
  	<!-- including style.css file -->
  	 <link rel="stylesheet" type="text/css" href="css/style.css" />
  	<!-- //style.css file -->
  	<!-- Fontawesome icons -->
  		<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
  	<!-- //Fontawesome icons -->
    <!-- slick css file -->
      <link rel="stylesheet" type="text/css" href="slick/slick.css" />
    <!-- //slick css file -->
  </head>
  <body>
  <!-- site header -->
    <?php require_once('includes/header.php'); ?>
  <!-- //site-header -->
  <!-- loading gif -->
    <?php require_once('includes/loader.php') ?>
  <!-- //loader -->
  <div class="container-fluid">
      
      <h1 class="max-width-950">Our Products</h1>
        
        <h2 class="max-width-950">Product 1</h2>
        <div class="product-desc">
          
          <div class="product-desc-top">
            <div class="max-width-950">
              <div class="col-md-3">
                <img src="images/marigold.png" class="img-responsive">
              </div>
              <div class="col-md-9">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                   tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                   quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                </p>
              </div>
            </div><!-- //max-width-950 -->
          </div><!-- product-desc-top -->
          <div class="row-fluid">
            
            <div class="col-md-12">
              <h3>Brief Description of the product</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
            </div><!-- //col-md-12 -->
            <div class="col-xs-12">
              <h3>Images of the product</h3>
              <div class="products-thumb text-center">
                <div><img src="images/milkstar.png"></div>
                <div><img src="images/milkstar.png"></div>
                <div><img src="images/milkstar.png"></div>
                <div><img src="images/milkstar.png"></div>
                <div><img src="images/milkstar.png"></div>
                <div><img src="images/milkstar.png"></div>
                <div><img src="images/milkstar.png"></div>
              </div><!-- //product-thumb -->
            </div>           
          
          </div><!-- //row-fluid -->
        
        </div><!-- //product-desc -->
      
      </div><!-- //product-item -->
      <div class="row-fluid product-item"></div>
      <div class="row-fluid product-item"></div>
  
  </div><!-- //container-fluid -->  
  <!-- site-footer -->
    <?php require_once('includes/footer.php'); ?>
  <!-- //site-footer -->

  <!-- jquery js file -->
    <script type="text/javascript" src="js/jquery.js"></script>
  <!-- //jquery file -->

  <!-- bootstrap js file -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
  <!-- /bootstrap js file -->
  <!-- Slick slider specific -->
    <script type="text/javascript" src="slick/slick.min.js"></script>
    <script type="text/javascript">
      $('.products-thumb').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true,
        autoplay: true,
        centerMode: true
      });
    </script>
  <!-- //slick slider specific files -->
  <!-- loader gif file -->
  <script type="text/javascript" src="js/loader.js"></script>
  <!-- //loader -->
  </body>
</html>