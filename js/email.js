//Script for contact us form 
  $(document).ready(function(){
    $('#submit').click(function(){
      var name = $('#name').val();
      var email = $('#email').val();
      var message = $('#message').val();
      $("#returnmessage").empty();// Empty previous error/success msgs
      //Checking for blank fields
      if(name == '' || email == '' || message == ''){
        alert('Please fill required fields');
      }
      else{
        //Returns successfull message when the entered info is sent successfully
        $.post("./email.php",{
          name1: name,
          email1: email,
          message1: message
        },function(data){
          $("#returnmessage").append(data);// Append return message to message paragraph
          if(data == "Your Query has been received, we will get in touch with you soon."){
            $("#form")[0].reset();// To reset the form fields on success
          }
        });
      }
    });
  });
//contact us form script ends