<!doctype html>
<html lang="en">
  <head>
  	<title>United Agro Inds</title>
  	
  	<!-- meta info -->
  	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<!-- /meta-info -->
    <link rel="shortcut icon" href="favicon.ico">
  	<!-- bootstrap css file -->
  	 <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
  	<!-- //bootstrap css file -->
  	
  	<!-- including style.css file -->
  	 <link rel="stylesheet" type="text/css" href="css/style.css" />
  	<!-- //style.css file -->
  	<!-- Fontawesome icons -->
  		<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
  	<!-- //Fontawesome icons -->
    <!-- slick css file -->
      <link rel="stylesheet" type="text/css" href="slick/slick.css" />
    <!-- //slick css file -->
    <!-- loading Montserrat font  -->
      <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <!-- //Mosterrat font -->
  </head>
  <body>
<div data-parallax="scroll" data-image-src="images/cow-lg-2.jpg">
  <!-- site header -->
    <?php require_once('includes/header.php'); ?>
  <!-- //site-header -->
  <!-- loading gif -->
    <?php require_once('includes/loader.php') ?>


  <div class="container-fluid">
    
    <h1 class="max-width-950 text-center products-h1">Our Products</h1> 
    <div class="row product-desc max-width-950">

      <h2 class="max-width-950 text-center">Marrie Gold Feed</h2>  
      <div class="max-width-950">
        <div class="col-md-2 col-md-push-2 product-desc-img">
          <img src="images/mari-gold.png" class="img-responsive">
        </div><!-- //col-md-3 -->
        <div class="col-md-6 col-md-push-2 product-desc-summary">
          <p>Marrie Gold Feed is prepared using latest technologies available and is
              a pure and balanced feed for cows and buffalloes.
          </p>
        </div><!-- //col-md-3 -->
      </div><!-- max-width-950 -->
    </div><!-- //row -->

    <div class="row product-brief max-width-950">
      
      <h3 class="max-width-950 text-center">Brief Description of the product</h3>
      <div class="col-md-12 product-brief-inner">
       
        <div class="max-width-950">
          <p>Marrie Gold Feed is prepared using latest technologies and is produced using
            raw material which comprises of <i>kaala binola</i>, <i>mustard cake</i>, <i>
            maize</i>, <i>jaon</i>, <i>rice bran</i>, <i>channa chilka</i> and <i>wheat</i>.
            Marrie Gold feed comprises of multi-vitamins, minerals, calcium, iron and other
            naturally occuring herbs. To preserve the Marrie Gold feed from harmful bacteria 
            and fungus, anti fungus and anti bacterial agents are added during its production.
          </p>
          <p>Marrie Gold feed goes through a series of laboratory test before its production
          and  distribution phase to keep a measure on the quality of the product and to adhere
          the product to the norms issued by the Govt. There is no need of any other product
          and Marrie Gold Feed can be fed to the cattle all alone. Marrie Gold Feed has different
          available types for specific use and to fulfill the specific demands of the customers.
          </p>
        </div><!-- //max-width-950 -->

      </div><!-- //product-brief-inner -->
    
    </div><!-- //product-brief -->
    <div class="row product-thumbs-odd max-width-950">
      <div class="text-center">
  
        <h3>Marrie Gold Products</h3> 
          <div class="col-xs-6 col-xs-offset-3 product-item">
              <h4 class="text-center">Marrie Gold Special Churi Blue</h4>
              <img src="images/marriegold/special-churi.png" class="img-responsive" />
          </div><!-- col-xs-12 -->
          <div class="col-xs-6 col-xs-offset-3 product-item">
              <h4 class="text-center">Marrie Gold Special Churi Green</h4>
              <img src="images/marriegold/special-churi-green.png" class="img-responsive"/>
          </div>
          <div class="col-xs-6 col-xs-offset-3 product-item">
            <h4 class="text-center">Marrie Gold Supreme</h4>
            <img src="images/marriegold/supreme-churi.png" class="img-responsive"/>
          </div>
          <div class="col-xs-6 col-xs-offset-3 product-item">
            <h4 class="text-center">Marrie Gold Diamond Churi</h4>
            <img src="images/marriegold/diamond-churi.png" class="img-responsive"/>
          </div>
          <div class="col-xs-6 col-xs-offset-3 product-item">
            <h4 class="text-center">Marrie Gold Hifi Churi</h4>
            <img src="images/marriegold/hifi-churi.png" class="img-responsive"/>
          </div>
          <div class="col-xs-6 col-xs-offset-3 product-item">
            <h4 class="text-center">Marrie Gold Natural Churi</h4>
            <img src="images/marriegold/natural-churi.png" class="img-responsive"/>
          </div>

      </div><!-- //max-width-950 -->
    </div><!-- //products-thumbs -->

    <div class="row product-desc max-width-950 product-desc-2">

      <h2 class="text-center">Milk Star Feed</h2>  
      <div class="max-width-950">
        <div class="col-md-2 col-md-push-2 product-desc-img">
          <img src="images/milk-star.png" class="img-responsive">
        </div><!-- //col-md-3 -->
        <div class="col-md-6 col-md-push-2 product-desc-summary">
          <p>Milk Star Feed is prepared using latest technologies available and is
              a pure and balanced feed for cows and buffalloes.
          </p>
        </div><!-- //col-md-3 -->
      </div><!-- max-width-950 -->
    
    </div><!-- //row -->
    <div class="row product-brief max-width-950 product-brief-2">
      
      <h3 class="text-center">Brief Description of the product</h3>
      <div class="col-md-12 product-brief-inner">
       
        <div class="max-width-950">
          <p>Milk Star Feed is prepared using latest technologies and is produced using
            raw material which comprises of <i>kaala binola</i>, <i>mustard cake</i>, <i>
            maize</i>, <i>jaon</i>, <i>rice bran</i>, <i>channa chilka</i> and <i>wheat</i>.
            Milk Star feed comprises of multi-vitamins, minerals, calcium, iron and other
            naturally occuring herbs. To preserve the Milk Star feed from harmful bacteria 
            and fungus, anti fungus and anti bacterial agents are added during its production.
          </p>
          <p>Milk Star feed goes through a series of laboratory test before its production
          and  distribution phase to keep a measure on the quality of the product and to adhere
          the product to the norms issued by the Govt. There is no need of any other product
          and Milk Star Feed can be fed to the cattle all alone. Milk Star Feed has different
          available types for specific use and to fulfill the specific demands of the customers.
          </p>
        </div><!-- //max-width-950 -->

      </div>
    
    </div><!-- //product-brief -->
    <div class="row milkstar-thumbs max-width-950">
      <div class="text-center">
  
        <h3>Milk Star Products</h3> 
          <div class="col-xs-6 col-xs-offset-3 product-item">
              <h4 class="text-center">Milk Star Special Churi Blue</h4>
              <img src="images/milkstar/special-churi-blue.png" class="img-responsive" />
          </div><!-- col-xs-12 -->
          <div class="col-xs-6 col-xs-offset-3 product-item">
              <h4 class="text-center">Milk Star Special Churi Green</h4>
              <img src="images/milkstar/special-churi-green.png" class="img-responsive"/>
          </div>
          <div class="col-xs-6 col-xs-offset-3 product-item">
            <h4 class="text-center">Milk Star Supreme Churi</h4>
            <img src="images/milkstar/supreme-churi.png" class="img-responsive"/>
          </div>
          <div class="col-xs-6 col-xs-offset-3 product-item">
            <h4 class="text-center">Milk Star Gold Churi</h4>
            <img src="images/milkstar/gold-churi.png" class="img-responsive"/>
          </div>
          <div class="col-xs-6 col-xs-offset-3 product-item">
            <h4 class="text-center">Milk Star Diamond Churi</h4>
            <img src="images/milkstar/diamond-churi.png" class="img-responsive"/>
          </div>
          <div class="col-xs-6 col-xs-offset-3 product-item">
            <h4 class="text-center">Milk Star Hifi Churi</h4>
            <img src="images/milkstar/hifi-churi.png" class="img-responsive"/>
          </div>
          <div class="col-xs-6 col-xs-offset-3 product-item">
            <h4 class="text-center">Milk Star Natural Churi</h4>
            <img src="images/milkstar/natural-churi.png" class="img-responsive"/>
          </div>

      </div><!-- //max-width-950 -->
    </div><!-- //products-thumbs -->

    <div class="row product-desc max-width-950">

      <h2 class="text-center">Mega Star Feed</h2>  
      <div class="max-width-950">
        <div class="col-md-2 col-md-push-2 product-desc-img">
          <img src="images/mega-star.png" class="img-responsive">
        </div><!-- //col-md-3 -->
        <div class="col-md-6 col-md-push-2 product-desc-summary">
          <p>Mega Star Feed is prepared using latest technologies available and is
              a pure and balanced feed for cows and buffalloes.
          </p>
        </div><!-- //col-md-3 -->
      </div><!-- max-width-950 -->
    
    </div><!-- //row -->
    <div class="row product-brief max-width-950">
      
      <h3 class="text-center">Brief Description of the product</h3>
      <div class="col-md-12 product-brief-inner">
       
        <div>
          <p>Mega Star Feed is prepared using latest technologies and is produced using
            raw material which comprises of <i>kaala binola</i>, <i>mustard cake</i>, <i>
            maize</i>, <i>jaon</i>, <i>rice bran</i>, <i>channa chilka</i> and <i>wheat</i>.
            Mega Star feed comprises of multi-vitamins, minerals, calcium, iron and other
            naturally occuring herbs. To preserve the Mega Star feed from harmful bacteria 
            and fungus, anti fungus and anti bacterial agents are added during its production.
          </p>
          <p>Mega Star feed goes through a series of laboratory test before its production
          and  distribution phase to keep a measure on the quality of the product and to adhere
          the product to the norms issued by the Govt. There is no need of any other product
          and Mega Star Feed can be fed to the cattle all alone. Mega Star Feed has different
          available types for specific use and to fulfill the specific demands of the customers.
          </p>
        </div><!-- //max-width-950 -->

      </div>
    
    </div><!-- //product-brief -->
    <div class="row product-thumbs-odd max-width-950">
      <div class="text-center">
  
        <h3>Mega Star Products</h3> 
          <div class="col-xs-6 col-xs-offset-3 product-item">
              <h4 class="text-center">Mega Star Special Churi Blue</h4>
              <img src="images/megastar/special-churi.png" class="img-responsive" />
          </div><!-- col-xs-12 -->
          <div class="col-xs-6 col-xs-offset-3 product-item">
            <h4 class="text-center">Mega Star Supreme Churi</h4>
            <img src="images/megastar/supreme-churi.png" class="img-responsive"/>
          </div>
          <div class="col-xs-6 col-xs-offset-3 product-item">
            <h4 class="text-center">Mega Star Gold Churi</h4>
            <img src="images/megastar/gold-churi.png" class="img-responsive"/>
          </div>
          <div class="col-xs-6 col-xs-offset-3 product-item">
            <h4 class="text-center">Mega Star Diamond Churi</h4>
            <img src="images/megastar/diamond-churi.png" class="img-responsive"/>
          </div>
          <div class="col-xs-6 col-xs-offset-3 product-item">
            <h4 class="text-center">Mega Star Hifi Churi</h4>
            <img src="images/megastar/hifi-churi.png" class="img-responsive"/>
          </div>
          <div class="col-xs-6 col-xs-offset-3 product-item">
            <h4 class="text-center">Mega Star Natural Churi Blue</h4>
            <img src="images/megastar/natural-churi.png" class="img-responsive"/>
          </div>

      </div><!-- //max-width-950 -->
    </div><!-- //products-thumbs -->

  </div><!-- //container-fluid -->  

  <!-- site-footer -->
    <?php require_once('includes/footer.php'); ?>
  <!-- //site-footer -->
</div> <!-- content wrap for parrallax effect -->
  <!-- jquery js file -->
    <script type="text/javascript" src="js/jquery.js"></script>
  <!-- //jquery file -->
    <!-- //loader -->
    <script type="text/javascript" src="js/loader.js"></script>
  <!-- //loader -->

  <!-- bootstrap js file -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
  <!-- /bootstrap js file -->
  <!-- Slick slider specific -->
    <script type="text/javascript" src="slick/slick.min.js"></script>
    <script type="text/javascript">
      $('.products-thumb').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true,
        autoplay: true,
        centerMode: true
      });
    </script>
  <!-- //slick slider specific files -->
   <!-- parallax effect -->
    <script language="javascript" src="js/parallax.min.js"></script>
  <!-- //parallax effect -->
  </body>
</html>