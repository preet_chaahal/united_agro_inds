<!doctype html>
<html lang="en">
  <head>
  	<title>United Agro Inds:: Some name</title>
  	
  	<!-- meta info -->
  	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<!-- /meta-info -->
    <link rel="shortcut icon" href="favicon.ico">
  	<!-- bootstrap css file -->
  	 <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
  	<!-- //bootstrap css file -->
  	
  	<!-- including style.css file -->
  	 <link rel="stylesheet" type="text/css" href="css/style.css" />
  	<!-- //style.css file -->
  	<!-- Fontawesome icons -->
  		<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
  	<!-- //Fontawesome icons -->
    <!-- loading Montserrat font  -->
      <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <!-- //Mosterrat font -->
  </head>
  <body>
  <div data-parallax="scroll" data-image-src="images/cow-lg-2.jpg">
    <!-- site header -->
      <?php require_once('includes/header.php'); ?>
    <!-- //site-header -->
    <!-- loading gif -->
    <?php require_once('includes/loader.php') ?>

  
  <div class="container-fluid">
    <div class="abt-img max-width-950">
        <div class="max-width-600">
          <img src="images/abt-us.jpg" class="img-responsive">
        </div>
    </div>
      <div class="max-width-950 row about-jumbotron">
        <h1 class="text-center">About us</h1>
        <p><b>United Agro Industries(U.A.I)</b> is the proud manufacturer of <em>SPECIAL CHURI AND
        HIGH QUALITY FEED</em>. U.A.I was established in 2000 and started its operations at village 
        Gondwal(Ludhiana). Right from the incorporation of the company the prior aim of U.A.I is
        to produce the best quality cattle feed. U.A.I proclaims that the cattle feed produced by
        any other company is of no match in comparison to the quality standards maintained during 
        the production of cattle feed by U.A.I.
        </p>
        <br>
        <p>The Company operates keeping in view the prosperity and demands of the customers. 
        Keeping the demands in mind cattle feed of different types as per requirements is prepared.
        A <em>helpline number</em> is available for the customers for any kind of
        assistance related to cattle feed or any other queries.</p>
        <p>Before the production of cattle feed the raw material is tested in the lab and the production
        process of the cattle feed is done as per the strict guidances and norms issued by the Government.
        </p>
        <br>
        <p>Company claims that the cattle feed improves the milk quality as well quantity. The operations 
        in the company are performed by experienced and trained staff and cattle feed is produced as per the 
        Government norms. The Cattle feed is produced using the latest technologies keeping in view the
        satisfaction of the customers.</p>
        <br>
        <p>The company has 3 Brands namely <b>Marrie Gold</b> , <b>Milk Star</b> and <b>Mega Star</b>.
        To provide knowledge about its products company has experienced salesman so that the customers
        can understand about the feed and clear any doubts of the customers.</p>  
      </div><!-- //jumbotron -->
  </div><!-- //container-fluid -->

    <!-- site-footer -->
      <?php require_once('includes/footer.php'); ?>
    <!-- //site-footer -->
</div> <!-- content wrap for parrallax effect -->
  <!-- jquery js file -->
    <script type="text/javascript" src="js/jquery.js"></script>
  <!-- //jquery file -->
  <!-- //loader -->
    <script type="text/javascript" src="js/loader.js"></script>
  <!-- //loader -->
  <!-- bootstrap js file -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
  <!-- /bootstrap js file -->
  <!-- loader gif file -->
    <!-- parallax effect -->
    <script language="javascript" src="js/parallax.min.js"></script>
  <!-- //parallax effect -->
  </body>
</html>